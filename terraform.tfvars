yandex-cloud-id  = "b1ght8tbalcdo7bk8qc5"
yandex-folder-id = "b1g99p5ni5kf58e3r44e"

#=========== service_account ==================================================
service-account-name  = "ops-account"
kms-provider-key-name = "kms key"

#============================= network ==========================#
network-name = "From Terraform"

#============================= subnet ===========================#
zone-a-v4-cidr-blocks = "10.1.0.0/16"
zone-b-v4-cidr-blocks = "10.2.0.0/16"
zone-c-v4-cidr-blocks = "10.3.0.0/16"


#==----------------------------k8s-cluster---------------------==#
controller-node-name        = "my cluster by artlers"
controller-node-description = "description on cluster artlers"
k8s-version                 = "1.23"
k8s-master-region           = "ru-central1"

#=========================k8s-workers=====================#
k8s-worker-platform          = "standard-v1"
k8s-worker-resources-memory  = 4
k8s-worker-resources-cores   = 2
k8s-worker-boot-disk-type    = "network-hdd"
k8s-worker-boot-disk-size    = 32
k8s-worker-public-ip         = true
k8s-worker-scheduling-policy = false

#========================= security group =======================#
white_ips_for_master = ["0.0.0.0/0"]