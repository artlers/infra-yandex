resource "yandex_kubernetes_node_group" "my_node_group_a" {
  cluster_id  = yandex_kubernetes_cluster.k8s-regional.id
  name        = "worker-a"
  description = "description"
  version     = var.k8s-version
  labels = {
    "key" = "value"
  }
  instance_template {
    platform_id = var.k8s-worker-platform
    name        = "worker-a-{instance.short_id}"
    network_interface {
      nat        = var.k8s-worker-public-ip
      subnet_ids = [yandex_vpc_subnet.mysubnet-a.id]
      security_group_ids = [
        yandex_vpc_security_group.internal.id,
      yandex_vpc_security_group.k8s-worker.id]
    }
    resources {
      memory = var.k8s-worker-resources-memory
      cores  = var.k8s-worker-resources-cores
    }
    boot_disk {
      type = var.k8s-worker-boot-disk-type
      size = var.k8s-worker-boot-disk-size
    }
    scheduling_policy {
      preemptible = var.k8s-worker-scheduling-policy
    }
  }
  scale_policy {
    auto_scale {
      min     = var.k8s-worker-scale-policy.min
      max     = var.k8s-worker-scale-policy.max
      initial = var.k8s-worker-scale-policy.initial
    }
  }
  allocation_policy {
    location {
      zone = "ru-central1-a"
    }
  }
}

resource "yandex_kubernetes_node_group" "my_node_group_b" {
  cluster_id  = yandex_kubernetes_cluster.k8s-regional.id
  name        = "worker-b"
  description = "description"
  version     = var.k8s-version
  labels = {
    "key" = "value"
  }
  instance_template {
    platform_id = var.k8s-worker-platform
    name        = "worker-b-{instance.short_id}"
    network_interface {
      nat                = var.k8s-worker-public-ip
      subnet_ids         = [yandex_vpc_subnet.mysubnet-b.id]
      security_group_ids = [yandex_vpc_security_group.internal.id, yandex_vpc_security_group.k8s-worker.id]
    }
    resources {
      memory = var.k8s-worker-resources-memory
      cores  = var.k8s-worker-resources-cores
    }
    boot_disk {
      type = var.k8s-worker-boot-disk-type
      size = var.k8s-worker-boot-disk-size
    }
    scheduling_policy {
      preemptible = var.k8s-worker-scheduling-policy
    }
  }
  scale_policy {
    auto_scale {
      min     = var.k8s-worker-scale-policy.min
      max     = var.k8s-worker-scale-policy.max
      initial = var.k8s-worker-scale-policy.initial
    }
  }
  allocation_policy {
    location {
      zone = "ru-central1-b"
    }
  }
}

/* resource "yandex_kubernetes_node_group" "my_node_group_c" {
  cluster_id  = yandex_kubernetes_cluster.k8s-regional.id
  name        = "worker-c"
  description = "description"
  version     = var.k8s-version
  labels = {
    "key" = "value"
  }
  instance_template {
    platform_id = var.k8s-worker-platform
    name        = "worker-a-{instance.short_id}"
    network_interface {
      nat                = var.k8s-worker-public-ip
      subnet_ids         = [yandex_vpc_subnet.mysubnet-c.id]
      security_group_ids = [yandex_vpc_security_group.internal.id, yandex_vpc_security_group.k8s-worker.id]
    }
    resources {
      memory = var.k8s-worker-resources-memory
      cores  = var.k8s-worker-resources-cores
    }
    boot_disk {
      type = var.k8s-worker-boot-disk-type
      size = var.k8s-worker-boot-disk-size
    }
    scheduling_policy {
      preemptible = var.k8s-worker-scheduling-policy
    }
  }
  scale_policy {
    auto_scale {
      min     = var.k8s-worker-scale-policy.min
      max     = var.k8s-worker-scale-policy.max
      initial = var.k8s-worker-scale-policy.initial
    }
  }
  allocation_policy {
    location {
      zone = "ru-central1-c"
    }
  }
}
*/
