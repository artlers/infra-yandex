terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  backend "s3" {
    endpoint                = "storage.yandexcloud.net"
    bucket                  = "artlers-test"
    region                  = "ru-central1"
    key                     = "terraform1.tfstate"
    shared_credentials_file = "storage.key"

    skip_region_validation      = true
    skip_credentials_validation = true
  }

}


provider "yandex" {
  cloud_id                 = var.yandex-cloud-id
  folder_id                = var.yandex-folder-id
  zone                     = var.default_zone
  service_account_key_file = "key.json"
}




