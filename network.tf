resource "yandex_vpc_network" "network-main" {
  name = var.network-name
}

resource "yandex_vpc_subnet" "mysubnet-a" {
  name           = "subnet-a"
  v4_cidr_blocks = [var.zone-a-v4-cidr-blocks]
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network-main.id
}

resource "yandex_vpc_subnet" "mysubnet-b" {
  name           = "subnet-b"
  v4_cidr_blocks = [var.zone-b-v4-cidr-blocks]
  zone           = "ru-central1-b"
  network_id     = yandex_vpc_network.network-main.id
}

resource "yandex_vpc_subnet" "mysubnet-c" {
  name           = "subnet-c"
  v4_cidr_blocks = [var.zone-c-v4-cidr-blocks]
  zone           = "ru-central1-c"
  network_id     = yandex_vpc_network.network-main.id
}
