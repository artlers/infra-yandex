variable "yandex-cloud-id" {
  description = "Yandex cloud id"
}

variable "yandex-folder-id" {
  description = "Yandex folder id"
}
variable "default_zone" {
  description = "The default zone"
  type        = string
  default     = "ru-cenral1-a"
}
#=========== service_account ==================================================
variable "service-account-name" {
  description = "Name of service account"
  type        = string
  default     = null
}

variable "kms-provider-key-name" {
  description = "KMS key name."
  type        = string
  default     = null
}
#============================ network ===========================================#

variable "network-name" {
  description = "The name of main network"
  type        = string
}

#============================ subnet ============================================#
variable "zone-a-v4-cidr-blocks" {
  description = "CIDR of ru-central1-a"
  type        = string
  default     = "10.1.0.0/16"
}
variable "zone-b-v4-cidr-blocks" {
  description = "CIDR of ru-central1-b"
  type        = string
  default     = "10.2.0.0/16"
}
variable "zone-c-v4-cidr-blocks" {
  description = "CIDR of ru-central1-c"
  type        = string
  default     = "10.3.0.0/16"
}

#==-----------------------------k8s-cluster------------------------------------==#
variable "controller-node-name" {
  description = "Name of control node"
  type        = string
  default     = "Zonal cluster k8s"
}

variable "controller-node-description" {
  description = "Control node description"
  type        = string
  default     = "Control node description"
}

variable "k8s-version" {
  description = "Version of kubernetes cluster"
  type        = string
  default     = "1.23"
}

variable "k8s-master-region" {
  description = "Region of master node"
  type        = string
  default     = "ru-central1"
}

variable "network_policy_provider" {
  description = "Network policy provider for the cluster. Possible values: CALICO."
  type        = string
  default     = "CALICO"
}

#===========================worker groups===========================================#
variable "k8s-worker-platform" {
  description = "Platform id "
  type        = string
  default     = "standard-v1"
}
variable "k8s-worker-resources-memory" {
  description = "Memory "
  type        = number
  default     = 4
}
variable "k8s-worker-resources-cores" {
  description = "Cores "
  type        = number
  default     = 2
}
variable "k8s-worker-boot-disk-type" {
  description = "Boot disk-type (hdd or ssd) "
  type        = string
  default     = "network-ssd"
}
variable "k8s-worker-boot-disk-size" {
  description = "Boot disk size "
  type        = number
  default     = 32
}
variable "k8s-worker-public-ip" {
  description = "Worker node public IP"
  type        = bool
  default     = true
}
variable "k8s-worker-scheduling-policy" {
  description = "Scheduling policy"
  type        = bool
  default     = false
}
variable "k8s-worker-scale-policy" {
  description = "Number of nodes"
  type        = map(any)
  default = {
    min     = 1
    max     = 3
    initial = 1
  }
}





#============================security group=========================================#
variable "white_ips_for_master" {
  type = list(string)
}

